<?php

// Included for development mode
// include_once('./dev.php');
 
// Composer autoloader
include_once('./vendor/autoload.php');

// Config file
include_once('./config.php');

// Routing aplication
include_once('./routing.php');