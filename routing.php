<?php

/**
 *   All application endpoints restfull api
 */
use Nonce\Nonce  as Nonce;
use Nonce\Config as NonceConfig;
use Klein\Klein  as Klein;

include_once './moduls/Task.modul.php';
include_once './moduls/User.modul.php';

$router = new Klein();

$router->respond('GET', '/task', function ($request, $response, $service) {
	beforeRestApi($request);

	$page = ($request->param("num") == null) ? 3 : $request->param("num");
	$order = ($request->param("order") == 'null') ? 'id' : $request->param("order");
	$offset = ($request->param("offset") == null) ? 0 : $request->param("offset");
	$nonce = $request->headers()['X-XSRF-TOKEN'];

	echo TaskModul::getTask($page, $order, $offset, $nonce);
	die();
});

$router->respond('GET', '/pages/count', function ($request, $response, $service) {
	beforeRestApi($request);
	$nonce = $request->headers()['X-XSRF-TOKEN'];

	echo TaskModul::getTaskCount($nonce);
	die();
});

$router->respond('PUT', '/task/[:id]', function ($request, $response, $service) {
	beforeRestApi($request);

	echo TaskModul::updateTask($request->id, json_decode($request->body()));
	die();
});

$router->respond('DELETE', '/task/[:id]', function ($request, $response, $service) {
	beforeRestApi($request);

	echo TaskModul::removeTask($request->id);
	die();
});

$router->respond('POST', '/task', function ($request, $response, $service) {
	beforeRestApi($request);

	echo TaskModul::createTask(json_decode($request->body()));
	die();
});

$router->respond('POST', '/task/preview', function ($request, $response, $service) {
	beforeRestApi($request);
	$nonce = $request->headers()['X-XSRF-TOKEN'];

	echo TaskModul::createPreviewTask(json_decode($request->body()), $nonce);
	die();
});

$router->respond('PUT', '/auth', function ($request, $response, $service) {
	beforeRestApi($request);

	$body = json_decode($request->body());
	$userModel = new UserModul();
	$user = $userModel->auth($body->login, $body->password);
	
	echo json_encode($user);
	die();
});

$router->respond('PUT', '/logout', function ($request, $response, $service) {
	beforeRestApi($request);

	$userModel = new UserModul();
	$result = $userModel->logout();
	
	echo json_encode($result);
	die();
});

$router->respond('PUT', '/check/auth', function ($request, $response, $service) {
	beforeRestApi($request);

	$body = json_decode($request->body());
	$userModel = new UserModul();
	$result = $userModel->checkAuth($body->token);
	
	echo json_encode($result);
	die();
});

$router->respond('GET', '*', function ($request, $response, $service) {
	setcookie(
		'XSRF-TOKEN',
		Nonce::create('ng-app'),
		time()+10000,
		NonceConfig::$COOKIE_PATH,
		NonceConfig::$COOKIE_DOMAIN,
		null,
		false
	);
	$service->render('app/dist/index.html');
	die();
});

$router->dispatch();

function beforeRestApi($request) {
	header("Content-type:application/json");

	if (!Nonce::verify($request->headers()['X-XSRF-TOKEN'], 'ng-app')) {
		header('HTTP/1.1 500 Internal Server Error');
		echo json_encode(array('error' => 'Не прошло nonce проверку'));
		die();
	}
}