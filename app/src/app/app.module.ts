import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SingFormComponent } from './sing-form/sing-form.component';
import { AdminService } from './admin.service';
import { SingleTaskComponent } from './single-task/single-task.component';
import { ArchiveTaskComponent } from './archive-task/archive-task.component';
import { TaskService } from './task.service';
import { PaginationComponent } from './pagination/pagination.component';
import { NewTaskComponent } from './new-task/new-task.component';


@NgModule({
  declarations: [
    AppComponent,
    SingFormComponent,
    SingleTaskComponent,
    ArchiveTaskComponent,
    PaginationComponent,
    NewTaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    FormsModule,
    HttpModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'XSRF-TOKEN',
      headerName: 'Xsrf'
    }),
    BrowserAnimationsModule
  ],
  providers: [
    AdminService,
    NgbActiveModal,
    TaskService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
