import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { trigger, style, animate, transition } from '@angular/animations';

import { Task } from '../task';
import { AdminService } from '../admin.service';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-single-task',
  templateUrl: './single-task.component.html',
  styleUrls: ['./single-task.component.scss'],
  animations: [
    trigger(
      'enterAnimation',
      [
        transition(
          ':enter',
          [
            style(
              {
                'transform': 'translateY(10%)',
                'opacity': 0
              }
            ),
            animate(
              '150ms 100ms',
              style(
                {
                  'transform': 'translateX(0)',
                  'opacity': 1
                }
              )
            )
          ]
        ),
        transition(
          ':leave',
          [
            style(
              {
                'position': 'absolute',
                'top': '0',
                'transform': 'scale(1)',
                'opacity': 1
              }
            ),
            animate(
              '500ms',
              style(
                {
                  'transform': 'scale(0)',
                  'opacity': 0
                }
              )
            )
          ]
        )
      ]
    )
  ]
})
export class SingleTaskComponent implements OnInit {
  @Input() task: Task;
  done: boolean;
  edit: boolean;
  send: boolean;
  error: boolean;
  delete: boolean;
  ajaxClass = {
    'btn-info': this.send,
    'btn-danger': this.error
  };

  doneText: string;
  editingTask: Task;

  constructor(
    private adminService: AdminService,
    private router: Router,
    private taskService: TaskService
  ) {}

  ngOnInit() {
    if (this.task.state === 0) {
      this.done = true;
    } else {
      this.done = false;
    }

    const self = this;
    this.editingTask = { ...self.task };
  }

  editTask() {
    const self = this;
    this.task = { ...self.editingTask };
    this.task.public_nonce = null;
    this.send = true;

    this.taskService
      .updateTask(this.task)
      .subscribe(() => {
        this.send = false;
        this.edit = false;
      }, () => {
        this.send = false;
        this.error = true;
        setTimeout(() => {
          this.error = false;
        }, 3000);
      });
  }

  resetTask() {
    const self = this;
    this.editingTask = { ...self.task };
  }

  remove() {
    this.taskService
      .removeTask(this.task)
      .subscribe(() => {
        this.delete = true;
        this.router.navigate(['', this.router.url]);
      });
  }

  uploadPhoto(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.editingTask.image = 'data:image/png;base64,' + reader.result.split(',')[1];
      };
    }
  }
}
