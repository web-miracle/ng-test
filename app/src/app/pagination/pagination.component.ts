import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { TaskService } from '../task.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  pageCount: number;
  navigationSubscription: any;
  page: number;

  constructor(
    public taskService: TaskService,
    private location: Location
  ) { }

  ngOnInit() {
    this.taskService
      .getPageCount()
      .subscribe((number) => {
        this.pageCount = number;
        this.page = parseInt(this.location.path().split('/')[2], 10);
      });
  }

  go($event, node) {
    this.taskService.setPage(node.page);
    this.page = node.page;
  }

}
