import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/**
 *   Authorized User Data Service
 *   @autor   Mishalov_Pavel
 *   @version 1.0
 *   @date    2018-04-30
 */
@Injectable()
export class AdminService {

  /**
   *   Admin user login
   *   @type {string}
   */
  admin: string;

  /**
   *   Meaning that the user is authorized
   *   @type {boolean}
   */
  login: boolean;

  constructor(public http: Http) {
    this.login = false;
  }

  /**
   *   Method for authorizing a user by login and password
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {string}       login    User login
   *   @param   {string}       password User password
   */
  singIn(login: string, password: string) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http
      .put('auth', JSON.stringify({'login': login, 'password': password}), { headers })
      .map((res) => res.json());
  }

  /**
   *   Verifies the user's authorization by the transmitted token
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {string}       token reference-token which is stored in cookies
   */
  checkAuth(token: string): void {
    const headers: Headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http
      .put('check/auth', JSON.stringify({'token': token}), { headers })
      .map((res) => res.json())
      .subscribe((res) => {
        this.setAdmin('admin');
      }, (error) => {
        this.unsetAdmin();
      });
  }

  /**
   *   Sets the authorized user
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {string}       login User login
   */
  setAdmin(login: string): void {
    this.admin = login;
    this.login = true;
  }

  /**
   *   Unsets the authorized user
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   */
  unsetAdmin(): void {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http
      .put('logout', JSON.stringify({}), { headers })
      .subscribe((res) => {
        this.admin = null;
        this.login = false;
      });
  }

}
