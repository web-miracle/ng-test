import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArchiveTaskComponent } from './archive-task/archive-task.component';

const routes: Routes = [
  {
    'path': 'page/:page',
    'component': ArchiveTaskComponent
  },
  {
    'path': '**',
    'redirectTo': 'page/1'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
