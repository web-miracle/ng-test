import { Component } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { AdminService } from '../admin.service';

@Component({
  selector: 'app-sing-form',
  templateUrl: './sing-form.component.html',
  styleUrls: ['./sing-form.component.scss']
})
export class SingFormComponent {
  closeResult: string;
  modal: any;
  error: boolean;

  constructor(
    public modalService: NgbModal,
    public activeModal: NgbActiveModal,
    public adminService: AdminService
  ) {
    this.error = false;
  }

  open(content) {
    this.modal = this.modalService.open(content);
    this.modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  private login(login: string, password: string, modal: any) {
    this.adminService
      .singIn(login, password)
      .subscribe((res) => {
        this.modal.close();
        this.adminService.setAdmin(res.login);
      }, (error) => {
        this.error = true;
        setTimeout(() => {
          this.error = false;
        }, 5000);
      });
  }

  private logout() {
    this.adminService
      .unsetAdmin();
  }
}
