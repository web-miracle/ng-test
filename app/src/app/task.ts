/**
 *   Task Interface
 */
export interface Task {
  'id'?: number;
  'name': string;
  'email': string;
  'content': string;
  'state': boolean | number;
  [propName: string]: any;
}
