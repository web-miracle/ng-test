import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';

import { TaskService } from '../task.service';
import { Task } from '../task';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss'],
  animations: [
    trigger(
      'enterAnimation',
      [
        transition(
          ':enter',
          [
            style(
              {
                'transform': 'translateY(10%)',
                'opacity': 0
              }
            ),
            animate(
              '150ms 100ms',
              style(
                {
                  'transform': 'translateX(0)',
                  'opacity': 1
                }
              )
            )
          ]
        ),
        transition(
          ':leave',
          [
            style(
              {
                'position': 'absolute',
                'top': '0',
                'transform': 'scale(1)',
                'opacity': 1
              }
            ),
            animate(
              '500ms',
              style(
                {
                  'transform': 'scale(0)',
                  'opacity': 0
                }
              )
            )
          ]
        )
      ]
    )
  ]
})
export class NewTaskComponent implements OnInit, OnDestroy {
  task: Task = {
    'name': '',
    'email': '',
    'content': '',
    'state': false
  };

  hints = {
    'name': 'Paste your name',
    'email': 'Paste your email',
    'content': 'Your new task',
  };

  imageSrc: string;

  preview = false;

  @Output() destroy = new EventEmitter<number>();
  constructor(public taskService: TaskService) { }

  ngOnInit() { }
  ngOnDestroy() { }

  onSubmit() {
    if (this.preview) {
      console.log('preview');
      this.taskService
          .addPreviewTask(this.task)
          .subscribe((res) => {
            if (res) {
              this.destroy.emit(res);
            }
          });
    }else {
      this.taskService.addTask(this.task)
          .subscribe((res) => {
            if (res) {
              this.destroy.emit(res);
            }
          });
    }
  }

  onCancel() {
    this.destroy.emit(null);
  }

  uploadPhoto(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      console.log(file);
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.task.image = 'data:' + file.type + ';base64,' + reader.result.split(',')[1];
      };
    }
  }

}
