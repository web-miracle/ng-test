import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

import { AdminService } from './admin.service';
import { TaskService } from './task.service';
import { Task } from './task';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  /**
   *   The string passed when the modal window is closed
   *   @type {string}
   */
  closeResult: string;

  /**
   *   The status of adding a new task
   *   @type {Boolean}
   */
  addNewTask = false;

  /**
   *   The new task id
   *   @type {number}
   */
  taskID: number;

  /**
   *   The output status of a message with the result of creating a new task
   *   @type {Boolean}
   */
  showHint = false;

  /**
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {NgbModal}     private modalService Modal window provided ng-bootstrap library
   *   @param   {Router}       private router       
   *   @param   {AdminService} private adminService A service that displays information about an authorized administrator
   *   @param   {TaskService}  public  taskService  Service that displays information about tasks
   */
  constructor(
    private modalService: NgbModal,
    private router: Router,
    private adminService: AdminService,
    public taskService: TaskService
  ) {}

  /**
   *   Modal window opening event
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {innerHTML}       content Modal window content
   */
  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  /**
   *   Modal window closing event
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {any}          reason
   *   @return  {string}
   */
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  /**
   *   When initialized, the authenticated user is verified by the values of the cookies
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   */
  ngOnInit() {
    const token = this.getCookie('REFRESH-TOKEN');
    const time = parseInt(this.getCookie('REFRESH-TIME'), 10);

    if (token && time > Date.now() / 1000) {
      this.adminService
        .checkAuth(token);
    } else if (token) {
      this.adminService
        .unsetAdmin();
    }
  }

  /**
   *   Task sorting tasks
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {string}       sort Field by which to sort
   */
  changeSort(sort: string) {
    this.taskService.sort = sort;
    this.router.navigate(['', this.router.url]);
  }

  /**
   *   Event for the completion of a new task
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {number}       id Id for eding task
   */
  onDestroy(id: number) {
    this.addNewTask = false;
    if (id) {
      this.router.navigate(['', this.router.url]);
      this.taskID = id;
      this.showHint = true;
    }
  }

  /**
   *   Cookie value retrieval function
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {string}       name Cookie name
   *   @return  {string}            Cookie value
   */
  private getCookie(name: string): string {
    const matches = document.cookie.match(new RegExp(
      '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }
}
