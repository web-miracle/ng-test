import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { Task } from './task';

/**
 *   Service for obtaining, creating, editing and deleting tasks
 *   @autor   Mishalov_Pavel
 *   @version [version]
 *   @date    2018-04-30
 */
@Injectable()
export class TaskService {

  /**
   *   Current task page
   *   @type {number}
   */
  page: number;

  /**
   *   A symptom of task sorting.
   *   With a value of false sorting by task id from the database
   *   @type {Boolean}
   */
  sort: string|boolean = false;

  /**
   *   Depends on the Router classes for setting the page for pagination
   *   and http for application requests
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {Router}       private router
   *   @param   {Http}         private http
   */
  constructor(
    private router: Router,
    private http: Http
  ) {}

  /**
   *   Getting application tasks
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {number}             page         Page for which you need to get tasks
   *   @param   {number}             tastkPerPage Number of tasks per page
   *   @return  {Observable<object>}              Flow with the object in which the received tasks are stored
   */
  getTasks(page: number, tastkPerPage: number): Observable<object> {
    const offset = ( (page - 1) * tastkPerPage );
    const order = (this.sort) ? this.sort : null;
    return this.http
        .get('task?offset=' + offset + '&order=' + order)
        .map((res) => res.json());
  }

  /**
   *   Number of tasks to determine the number of pages
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @return  {Observable<number>} Flow with the number tasks
   */
  getPageCount(): Observable<number> {
    return this.http
      .get('pages/count')
      .map((res) => res.json());
  }

  /**
   *   Setting the current task page and transition to it
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {number}       page The value of the page
   */
  setPage(page: number): void {
    if (this.page === page) {
      return;
    }

    this.router
      .navigateByUrl('page/' + page)
      .then((e) => {
        this.page = page;
      });
  }

  /**
   *   Adding a new task
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {Task}               task
   *   @return  {Observable<number>}      A stream with the id value of the created page
   */
  addTask(task: Task): Observable<number> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http
          .post('task', JSON.stringify(task), { headers })
          .map((res) => res.json());
  }

  /**
   *   Adding a new preview task
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {Task}               task
   *   @return  {Observable<number>}      A stream with the id value of the created page
   */
  addPreviewTask(task: Task): Observable<number> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http
          .post('task/preview', JSON.stringify(task), { headers })
          .map((res) => res.json());
  }

  /**
   *   Update task for the transmitted id
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {Task}            task
   *   @return  {Observable<any>}      Flow with a modified task
   */
  updateTask(task: Task): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http
          .put('task/' + task.id, JSON.stringify(task), { headers })
          .map((res) => res.json());
  }

  /**
   *   Deleting a task
   *   @autor   Mishalov_Pavel
   *   @version 1.0
   *   @date    2018-04-30
   *   @param   {Task}            task
   *   @return  {Observable<any>}
   */
  removeTask(task: Task): Observable<any> {
    const headers = new Headers();

    return this.http
          .delete('task/' + task.id)
          .map((res) => res.json());
  }
}
