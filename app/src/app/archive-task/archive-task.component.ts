import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

import { SingleTaskComponent } from '../single-task/single-task.component';
import { TaskService } from '../task.service';

/**
 *   Tasks component on the page
 *   @autor   Mishalov_Pavel
 *   @version 1.0
 *   @date    2018-04-30
 */
@Component({
  selector: 'app-archive-task',
  templateUrl: './archive-task.component.html',
  styleUrls: ['./archive-task.component.scss']
})
export class ArchiveTaskComponent implements OnInit{

  /**
   *   Tasks
   *   @type {any}
   */
  tasks: any;

  /**
   *   Value of wait answer for tasks data
   *   @type {Boolean}
   */
  wait = false;

  constructor(
    public taskService: TaskService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.wait) {
      return;
    }

    this.wait = !this.wait;
    this.route
        .params
        .map((res: Params) => {
          if (res['page']) {
            this.taskService.page = res['page'];
          } else {
            this.taskService.page = 1;
          }

          return res['page'];
        })
        .mergeMap((page: number) => {
          return this.taskService.getTasks(page, 3);
        })
        .subscribe((res: object) => {
          this.tasks = [];

          for (let item in res) {
            this.tasks.push(res[item]);
          }

          this.wait = false;
        });
  }
}
