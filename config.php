<?php

use Nonce\Config as NonceConfig;
use RedBeanPHP\R as DB;

// Nonce configuration
NonceConfig::$SALT = 'wL`i%aQh4e|0Pg`7Nr`v|8cx(wzH>4+B<7GHNO]|1wXQ8XETfx+/ZnSklrr&YK~W';
NonceConfig::$COOKIE_PATH = '/';
NonceConfig::$COOKIE_DOMAIN = $_SERVER['HTTP_HOST'];

// DB configuration
$host = 'localhost';
$dbname = 'angular-test';
$dbuser = 'root';
$dbpass = '1';

DB::setup('mysql:host=' . $host . ';dbname=' . $dbname, $dbuser, $dbpass);