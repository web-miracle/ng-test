<?php
use RedBeanPHP\R as DB;

/**
 *   Module for obtaining information about tasks
 */
class TaskModul
{
	
	function __construct()
	{
		
	}

	/**
	 * getTask
	 * @description Getting tastks from database
	 * @author Pavel Mishalov
	 * @email       ekonomikal@mail.ru
	 * @date        2018-04-28
	 * @version     0.01
	 * @param       integer              $num    number of tasks, which is needed
	 * @param       string               $order  column for ordering by
	 * @param       integer              $offset offset from begining
	 * @return      json                      	 array of task objects
	 */
	public static function getTask($num = 3, $order = "id", $offset = 0, $nonce = NULL)
	{
		$tasks = DB::getAll( "SELECT * FROM `tasks` WHERE `public_nonce`='$nonce' OR `public_nonce` IS NULL ORDER BY `" .$order . "` ASC LIMIT $num OFFSET $offset" );
		return json_encode( $tasks );
	}

	/**
	 * getTaskCount
	 * @description Getting tasks count
	 * @author Pavel Mishalov
	 * @email       ekonomikal@mail.ru
	 * @date        2018-04-28
	 * @version     0.01
	 * @return      json               number of tasks
	 */
	public static function getTaskCount($nonce) {
		$tasks_count = DB::count( 'tasks', ' WHERE `public_nonce`="' . $nonce . '" OR `public_nonce` IS NULL' );
		return json_encode( ceil( $tasks_count ) );
	}

	/**
	 * updateTask
	 * @description updating task fields
	 * @author Pavel Mishalov
	 * @email       ekonomikal@mail.ru
	 * @date        2018-04-28
	 * @version     0.01
	 * @param       number               $id     which task is being updated
	 * @param       array                $fields array of fields for update in current task
	 * @return      number                       id of updated task
	 */
	public static function updateTask($id, $fields = array()) {
		$task = DB::load( 'tasks', $id );
		if ($task->id != 0){
			foreach ($fields as $key => $value) {
				$task->$key = $value;
			}
			$task->public_nonce = NULL;
			DB::store( $task );
		}
		return $id;
	}

	/**
	 * removeTask
	 * @description remove task fields
	 * @author Pavel Mishalov
	 * @email       ekonomikal@mail.ru
	 * @date        2018-04-28
	 * @version     0.01
	 * @param       number               $id     which task is being removed
	 * @return      number                       id of remove task
	 */
	public static function removeTask($id) {
		$task = DB::load( 'tasks', $id );
		if ($task->id != 0){
			DB::trash( $task );
		}
		return $id;
	}

	/**
	 * CreateTask
	 * @description creating new task
	 * @author Pavel Mishalov
	 * @email       ekonomikal@mail.ru
	 * @date        2018-04-28
	 * @version     0.01
	 * @param       array               $fields array of fields to be set in new task
	 * @return      number                      id of created task
	 */
	public static function createTask($fields) {
		$task = DB::dispense('tasks');

		foreach ($fields as $key => $value) {
			$task->$key = $value;
		}
		
		return DB::store( $task );
	}

	/**
	 * CreateTask preview task
	 *
	 * Method create task with preview field as $user_nonce
	 * for the user to preview their own not yet published
	 * This method exist for see previews tasks 
	 * 
	 * @author      Pavel Mishalov
	 * @email       ekonomikal@mail.ru
	 * @date        2018-04-28
	 * @version     1.0
	 * @param       array               $fields array of fields to be set in new task
	 * @param       string              $user_nonce user sesion nonce
	 * @return      number                      id of created task
	 */
	public static function createPreviewTask($fields, $user_nonce) {
		$task = DB::dispense('tasks');

		foreach ($fields as $key => $value) {
			$task->$key = $value;
		}
		$task->public_nonce = $user_nonce;
		
		return DB::store( $task );
	}
}