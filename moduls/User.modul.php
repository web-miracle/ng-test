<?php
use RedBeanPHP\R as DB;

/**
*   Task module for obtaining information about the users
*/
class UserModul
{
	
	function __construct()
	{
		// Token lifetime for authorization
		$this->time = 60 * 60 *5;

		// Impurity to access token
		$this->sallAccess = 'fm,45on8v`nUHRIUN$J-nvrerehfg';
		
		// Impurity to refresh token
		$this->sallRefresh = 'fmggmoijn9pNFRHF*ER&HREUIFNDBCJH!OUYSGDW&G';
		
		// Path to cookie
		$this->cookiePath = '/';
		
		// Domain to cookie
		$this->cookieDomain = $_SERVER['HTTP_HOST'];
	}

	/**
	 *   User authorization
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-04-30
	 *   @param   string         $login    User login
	 *   @param   string         $password User password
	 *   @return                           if check user return user login
	 */
	public function auth($login = "", $password = "")
	{
		$user = DB::find(
					'users',
					'login = :login && password = :password',
					array(
						':login'    => $login,
						':password' => $password
					)
				);
		if (empty($user)) {
			header('HTTP/1.1 500 Internal Server Error');
			return array('error' => 'Нет такого пользователя');
		}

		return $this->setToken(key($user));
	}

	/**
	 *   Check user authorization
	 *   @autor   Mishalov_Pavel
	 *   @version 1.0
	 *   @date    2018-04-30
	 *   @param   string         $token Access token for user
	 *   @return                           if check user return user login
	 */
	public function checkAuth($token = "")
	{
		$user = DB::find(
					'users',
					'reffresh_token = :token',
					array(
						':token'    => $token
					)
				);
		if (empty($user)) {
			header('HTTP/1.1 500 Internal Server Error');
			$this->removeToken(key($user));
			return array('error' => 'Нет такого пользователя');
		}

		return $this->checkUser(key($user));
	}

	/**
	 *   Logout and delete all authorization cookies
	 *   @autor   Mishalov_Pavel
	 *   @version [version]
	 *   @date    2018-04-30
	 *   @return  [type]         [description]
	 */
	public function logout() {
		$user = DB::find(
					'users',
					'access_token = :token',
					array(
							':token' => $_COOKIE['ACCESS-TOKEN']
						)
				);

		return $this->removeToken(key($user));
	}

	private function checkUser($user) {
		$field = DB::load('users', $user)->access_token;

		if ($field == $_COOKIE['ACCESS-TOKEN']) {
			return $this->setToken($user);
		} else {
			return $this->removeToken($user);
		}
	}

	private function setToken($user)
	{
		$user = DB::load('users', $user);

		$access = base64_encode( $user->login . $user->password . $this->sallAccess );
		$refresh = base64_encode( $user->login . $user->password . $this->sallRefresh );
		$time = time() + $this->time;

		$user->access_token = $access;
		$user->reffresh_token = $refresh;

		$this->setAuthCookie($access, $refresh);

		DB::store($user);
		return array('login' => $user->login);
	}

	private function removeToken($user)
	{
		$user = DB::load('users', $user);

		$user->access_token = '';
		$user->reffresh_token = '';

		$this->removeAuthCookie();

		DB::store($user);
		return true;
	}

	private function setAuthCookie($access = '', $refresh = '') {
		setcookie(
			'ACCESS-TOKEN',
			$access,
			time() + $this->time,
			$this->cookiePath,
			$this->cookieDomain,
			null,
			true
		);

		setcookie(
			'REFRESH-TOKEN',
			$refresh,
			time() + $this->time,
			$this->cookiePath,
			$this->cookieDomain,
			null,
			false
		);

		setcookie(
			'REFRESH-TIME',
			time() + $this->time,
			time() + $this->time,
			$this->cookiePath,
			$this->cookieDomain,
			null,
			false
		);
	}

	private function removeAuthCookie() {
		setcookie(
			'ACCESS-TOKEN',
			'',
			time() - $this->time,
			$this->cookiePath,
			$this->cookieDomain,
			null,
			true
		);

		setcookie(
			'REFRESH-TOKEN',
			'',
			time() - $this->time,
			$this->cookiePath,
			$this->cookieDomain,
			null,
			false
		);

		setcookie(
			'REFRESH-TIME',
			$access,
			time() - $this->time,
			$this->cookiePath,
			$this->cookieDomain,
			null,
			false
		);
	}
}